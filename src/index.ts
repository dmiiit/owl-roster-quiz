/* UTILITY FUNCTIONS */

function sortBy<Element, Field>(f: (el: Element) => Field, els: Element[]): Element[] {
    els.sort((a, b) => {
        const fa = f(a);
        const fb = f(b);
        if      (fa < fb) { return -1 }
        else if (fa > fb) { return  1 }
        else              { return  0 }
    });
    return els;
}

function normalizeHandle(handle: string): string {
    return handle
        .trim()
        .replace(/0/g, "o") // thanks, BigG00se
        .replace(/1/g, "l") // thanks, Marve1
        .replace(/2/g, "e") // thanks, Viol2t
        .replace(/3/g, "e") // thanks, Fl0w3R
        .toLowerCase();
}

function prettyDuration(seconds: number): string {
    if (seconds <= 0) {
        return "0s";
    }

    const minute = 60;
    const hour = 60 * minute;

    let pretty = "";
    let remainder = Math.round(seconds);
    if (seconds > hour) {
        const hours = Math.floor(remainder / hour);
        remainder = remainder % hour;
        pretty = `${hours}h `;
    }

    if (seconds > minute) {
        const minutes = Math.floor(remainder / minute);
        remainder = remainder % minute;
        pretty = `${pretty}${minutes}min `;
    }

    pretty = `${pretty}${remainder}s`;
    return pretty;
}

/* DATA MODEL */

enum Role {
    Offense = "offense",
    Tank    = "tank",
    Support = "support",
}

function roleIcon(role: Role): SVGSVGElement {
    const SVG = "http://www.w3.org/2000/svg";
    const XLINK = "http://www.w3.org/1999/xlink";

    const makeSvg = (roleClass: string, iconId: string) => {
        const svg = document.createElementNS(SVG, "svg");
        svg.setAttribute("xmlns", SVG);
        svg.classList.add("role-icon", roleClass);

        const use = document.createElementNS(SVG, "use");
        use.setAttributeNS(XLINK, "href", iconId);
        svg.appendChild(use);

        return svg;
    }

    switch (role) {
        case Role.Offense:
            return makeSvg("role-offense", "#role-icon-offense");
        case Role.Tank:
            return makeSvg("role-tank", "#role-icon-tank");
        case Role.Support:
            return makeSvg("role-support", "#role-icon-support");
    }
}

type TeamName = string;
type PlayerHandle = string;

interface Team {
    id: number;
    name: TeamName;
    abbreviatedName: string;
    primaryColor: string;
    secondaryColor: string;
    icon: string;
}

interface TeamEntry {
    team: Team;
}

interface PlayerAttributes {
    role: Role;
    heroes?: string[];
}

interface Player {
    id: number;
    name: PlayerHandle;
    givenName: string;
    familyName: string;
    teams: TeamEntry[];
    attributes: PlayerAttributes;
}

class Color {
    r: number;
    g: number;
    b: number;
    a: number;

    constructor(r: number, g: number, b: number, a?: number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a || 1.0;
    }

    static fromHex(hex: string): Color {
        const r = parseInt(hex.slice(0, 2), 16);
        const g = parseInt(hex.slice(2, 4), 16);
        const b = parseInt(hex.slice(4, 6), 16);
        return new Color(r, g, b);
    }

    asCss(alpha?: number): string {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${alpha || this.a || 1.0})`;
    }

    luminance(): number {
        return (0.299 * this.r + 0.587 * this.g + 0.114 * this.b) / 255.0;
    }
}

/* AJAX */

function loadRosters(cb: (teamInfo: Map<TeamName, [Team, Player[]]>) => void) {
    fetch("https://api.overwatchleague.com/players?locale=en_US")
        .then(resp => resp.json())
        .then(json => {
            const players = <Player[]>json.content;
            const teamInfo = new Map<TeamName, [Team, Player[]]>();
            for (const player of players) {
                const team = player.teams[0].team;
                const roster = teamInfo.get(team.name) || [team, []];
                roster[1].push(player);
                teamInfo.set(team.name, roster);
            }

            for (const [, [, roster]] of teamInfo) {
                sortBy(p => p.name.toLowerCase(), roster);
            }

            cb(teamInfo);
        });
}

/* LOCAL STORAGE */
type Percentage = number;
type ElapsedSeconds = number;

function getPersonalBest(): [Percentage, ElapsedSeconds] | null {
    try {
        const personalBestPctStr = window.localStorage.getItem("personal-best-pct");
        const personalBestTimeStr = window.localStorage.getItem("personal-best-time");
        if (personalBestPctStr && personalBestTimeStr) {
            const personalBestPct = parseFloat(personalBestPctStr);
            const personalBestTime = parseFloat(personalBestTimeStr);
            return [personalBestPct, personalBestTime];
        }
    } catch (ex) {
        console.log("Local storage error, unable to retrieve personal best: ", ex);
    }
    return null;
}

function setPersonalBest(pct: Percentage, time: ElapsedSeconds) {
    try {
        const now = new Date();
        window.localStorage.setItem("personal-best-pct", pct.toString());
        window.localStorage.setItem("personal-best-time", time.toString());
        window.localStorage.setItem("personal-best-timestamp", now.toISOString());
    } catch (ex) {
        console.log("Local storage error, unable to persist personal best: ", ex);
    }
}

function resetPersonalBest() {
    try {
        window.localStorage.clear()
    } catch (ex) {
        console.log("Local storage error, unable to clear personal best: ", ex);
    }
}

/* INITIALIZATION */

let teamRosters: Map<TeamName, Player[]> = new Map();

function init() {
    const root = <HTMLDivElement>document.getElementById("root");
    const timeLimit = <HTMLSelectElement>document.getElementById("time-limit");
    const timer = <HTMLSpanElement>document.getElementById("time-remaining");
    const giveUpButton = <HTMLButtonElement>document.getElementById("button-give-up");
    const personalBest = <HTMLSpanElement>document.getElementById("personal-best-value");
    const personalBestReset = <HTMLAnchorElement>document.getElementById("personal-best-reset");
    const startButton = <HTMLButtonElement>document.getElementById("button-start");

    const personalBestRecord = getPersonalBest();
    if (personalBestRecord) {
        const [personalBestPct, personalBestTime] = personalBestRecord;
        personalBest.innerHTML = `${personalBestPct.toFixed(1)}% in ${prettyDuration(personalBestTime)}`;
        personalBestReset.style.display = "inline";
    }

    personalBestReset.onclick = (ev) => {
        resetPersonalBest();
        personalBest.innerHTML = "none";
        personalBestReset.style.display = "none";
        ev.preventDefault();
    };

    startButton.onclick = () => {
        loadRosters(rosters => {
            const startTime = Date.now();
            let timerHandle: number | undefined;

            giveUpButton.disabled = false;

            const finalizeAndPostStats = (msg: string) => {
                const endTime = Date.now();
                const elapsedSeconds = (endTime - startTime) / 1000.0;
                clearInterval(timerHandle);

                const playerSlots = document.getElementsByClassName("player-slot");
                for (const slot of Array.from(playerSlots)) {
                    if (!slot.classList.contains("correct")) {
                        slot.classList.remove("unknown");
                        slot.classList.add("incorrect");
                    }
                }

                const playerInputs = <HTMLCollectionOf<HTMLInputElement>>document.getElementsByClassName("guess");
                for (const input of Array.from(playerInputs)) {
                    input.remove();
                }
                giveUpButton.disabled = true;

                root.appendChild(document.createElement("hr"));

                const outcome = document.createElement("h2");
                outcome.innerHTML = msg;
                root.appendChild(outcome);

                const stats = document.createElement("table");
                stats.id = "stats";

                const headerRow = document.createElement("tr");

                const headerTeam = document.createElement("th");
                headerTeam.classList.add("stat-key");
                headerTeam.innerHTML = "Team";
                headerRow.appendChild(headerTeam);

                const headerResult = document.createElement("th");
                headerResult.classList.add("stat-value");
                headerResult.innerHTML = "Result";
                headerRow.appendChild(headerResult);

                stats.appendChild(headerRow);

                let totalGuessed = 0;
                let totalPossible = 0;

                for (const [teamName, [gotem, total]] of guesses) {
                    totalGuessed += gotem;
                    totalPossible += total;

                    const row = document.createElement("tr");

                    const teamCell = document.createElement("td");
                    teamCell.classList.add("stat-key");
                    teamCell.innerHTML = teamName;
                    row.appendChild(teamCell);

                    const resultCell = document.createElement("td");
                    resultCell.classList.add("stat-value");
                    resultCell.innerHTML = `${gotem}/${total}`;
                    row.appendChild(resultCell);

                    stats.appendChild(row);
                }

                // Totals
                const totalRow = document.createElement("tr");

                const totalCell = document.createElement("th");
                totalCell.classList.add("stat-key");
                totalCell.innerHTML = "Total";
                totalRow.appendChild(totalCell);

                const guessPercentage = 100.0 * totalGuessed / totalPossible;
                const totalResult = document.createElement("th");
                totalResult.classList.add("stat-value");
                totalResult.innerHTML = `${totalGuessed}/${totalPossible} (${guessPercentage.toFixed(1)}%)`
                totalRow.appendChild(totalResult);

                stats.appendChild(totalRow);

                // Time elapsed
                const timeElapsedRow = document.createElement("tr");

                const timeElapsedCell = document.createElement("td");
                timeElapsedCell.classList.add("stat-key");
                timeElapsedCell.innerHTML = "Time elapsed";
                timeElapsedRow.appendChild(timeElapsedCell);

                const timeElapsedValue = document.createElement("td");
                timeElapsedValue.classList.add("stat-value");
                timeElapsedValue.innerHTML = prettyDuration(elapsedSeconds);
                timeElapsedRow.appendChild(timeElapsedValue);

                stats.appendChild(timeElapsedRow);

                root.appendChild(stats);

                // Update personal best if necessary
                const personalBestRecord = getPersonalBest();
                if (personalBestRecord) {
                    const [personalBestPct, personalBestTime] = personalBestRecord;

                    if (guessPercentage >= personalBestPct) {
                        setPersonalBest(guessPercentage, elapsedSeconds);
                        personalBest.innerHTML = `${guessPercentage.toFixed(1)}% in ${prettyDuration(elapsedSeconds)}`;
                        personalBestReset.style.display = "inline";
                    } else if (guessPercentage == personalBestPct) {
                        const newBestTime = Math.min(personalBestTime, elapsedSeconds);
                        setPersonalBest(guessPercentage, newBestTime);
                        personalBest.innerHTML = `${guessPercentage.toFixed(1)}% in ${prettyDuration(newBestTime)}`;
                        personalBestReset.style.display = "inline";
                    }
                } else {
                    setPersonalBest(guessPercentage, elapsedSeconds);
                    personalBest.innerHTML = `${guessPercentage.toFixed(1)}% in ${prettyDuration(elapsedSeconds)}`;
                    personalBestReset.style.display = "inline";
                }

                stats.scrollIntoView({ behavior: "smooth" });
            };

            const updateTimer = (timeLimitMillis: number) => {
                const now = Date.now();
                const deadline = startTime + timeLimitMillis;
                if (now >= deadline) {
                    timer.innerHTML = "0s";
                    finalizeAndPostStats("Time's up!")
                } else {
                    const remaining = deadline - now;
                    timer.innerHTML = prettyDuration(remaining / 1000);
                }
            };

            giveUpButton.onclick = () => {
                finalizeAndPostStats("Mada mada!");
            };

            const timeLimitMillis: number | null = (parseInt(timeLimit.value, 10) * 60 * 1000) || null;

            if (timeLimitMillis) {
                timer.innerHTML = prettyDuration(timeLimitMillis / 1000);
                timerHandle = setInterval(updateTimer, 1000, timeLimitMillis);
            } else {
                timer.innerHTML = "none";
            }
            timeLimit.remove();

            // Clear the starting controls
            root.innerHTML = "";

            const sorted: [Team, Player[]][] = [];
            for (const [team, roster] of rosters.values()) {
                sorted.push([team, roster]);
            };
            sortBy(a => a[0].name.toLowerCase(), sorted);

            type GuessTotal = number;
            type GuessCurrent = number;
            const guesses = new Map<TeamName, [GuessCurrent, GuessTotal]>();

            let focused = false;
            let tabIndex = 1;
            for (const [team, roster] of sorted) {
                const primaryColor = Color.fromHex(team.primaryColor);
                const secondaryColor = Color.fromHex(team.secondaryColor);

                const teamSection = document.createElement("section");
                teamSection.classList.add("team");
                teamSection.style.borderLeftColor   = primaryColor.asCss();
                teamSection.style.borderTopColor    = primaryColor.asCss();
                teamSection.style.borderRightColor  = secondaryColor.asCss();
                teamSection.style.borderBottomColor = secondaryColor.asCss();

                const teamName = document.createElement("h2");
                teamName.classList.add("team-name");
                teamName.innerHTML = team.name;
                teamSection.appendChild(teamName);

                guesses.set(team.name, [0, roster.length]);

                const playerElements = new Map<PlayerHandle, [Player, HTMLLIElement]>();

                const playerNameInput = document.createElement("input");
                playerNameInput.classList.add("guess");
                playerNameInput.type = "text";
                playerNameInput.tabIndex = tabIndex;
                tabIndex += 1;

                playerNameInput.oninput = () => {
                    const key = normalizeHandle(playerNameInput.value);
                    const entry = playerElements.get(key);
                    if (entry && entry[1].classList.contains("unknown")) {
                        const [player, el] = entry;

                        el.innerHTML = player.name;
                        el.classList.remove("unknown");
                        el.classList.add("correct");

                        let guess = guesses.get(team.name) || [0, roster.length];
                        guess[0] += 1;

                        const [guessCount, guessTotal] = guess;
                        guessCountEl.innerHTML = `${guessCount}/${guessTotal}`;

                        if (guessCount == guessTotal) {
                            // Focus next input element, if any
                            const inputs = <HTMLCollectionOf<HTMLInputElement>>document.getElementsByClassName("guess");
                            let foundSelf = false;
                            for (const input of Array.from(inputs)) {
                                if (foundSelf) {
                                    input.focus();
                                    break;
                                }

                                if (input == playerNameInput) {
                                    foundSelf = true;
                                }
                            }

                            playerNameInput.remove();
                            guessCountEl.classList.add("all-correct");

                            for (const [, [gotem, total]] of guesses) {
                                if (gotem < total) {
                                    return;
                                }
                            }

                            // Guessed everything
                            finalizeAndPostStats("Perfect!");
                        } else {
                            playerNameInput.value = "";
                        }
                    }
                };
                teamSection.appendChild(playerNameInput);

                const guessCountEl = document.createElement("span");
                guessCountEl.classList.add("guess-counter");
                const [guessCount, guessTotal] = guesses.get(team.name)!;
                guessCountEl.innerHTML = `${guessCount}/${guessTotal}`;
                teamSection.appendChild(guessCountEl);

                const rosterEl = document.createElement("div");
                rosterEl.classList.add("roster");

                // DPS
                const dpsPlayers = document.createElement("div")
                dpsPlayers.classList.add("role");
                dpsPlayers.appendChild(roleIcon(Role.Offense));
                const dpsPlayersList = document.createElement("ul");
                dpsPlayers.appendChild(dpsPlayersList);
                rosterEl.appendChild(dpsPlayers);

                // Tank
                const tankPlayers = document.createElement("div");
                tankPlayers.classList.add("role");
                tankPlayers.appendChild(roleIcon(Role.Tank));
                const tankPlayersList = document.createElement("ul");
                tankPlayers.appendChild(tankPlayersList);
                rosterEl.appendChild(tankPlayers);

                // Support
                const supportPlayers = document.createElement("div");
                supportPlayers.classList.add("role");
                supportPlayers.appendChild(roleIcon(Role.Support));
                const supportPlayersList = document.createElement("ul");
                supportPlayers.appendChild(supportPlayersList);
                rosterEl.appendChild(supportPlayers);

                for (const player of roster) {
                    const el = document.createElement("li");
                    const playerName = document.createElement("span");
                    playerName.classList.add("player-name");
                    playerName.innerHTML = player.name;
                    el.appendChild(playerName);
                    el.classList.add("player-slot", "unknown");
                    switch (player.attributes.role) {
                        case Role.Offense:
                            dpsPlayersList.appendChild(el);
                            break;
                        case Role.Tank:
                            tankPlayersList.appendChild(el);
                            break;
                        case Role.Support:
                            supportPlayersList.appendChild(el);
                            break;
                    }

                    const key = normalizeHandle(player.name);
                    playerElements.set(key, [player, el]);
                }

                teamSection.appendChild(rosterEl);
                root.appendChild(teamSection);
                if (!focused) {
                    playerNameInput.focus();
                    focused = true;
                }
            }
        });
    };
}
