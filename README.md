# Overwath League Roster Quiz

See how many Overwatch League players you can name in 10 minutes.

![Screenshot](image.png)

## Build
Requirements: [TypeScript compiler](https://www.typescriptlang.org).

Run
```
tsc
```
to build the JavaScript code from TypeScript sources. Open `index.html` in a web browser to see the result.

## License
This software is released into the [public domain](LICENSE).
